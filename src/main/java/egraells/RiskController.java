package egraells;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;

@RestController
public class RiskController {

    @RequestMapping(value = "/pro/get", method = RequestMethod.GET, produces = "application/json")
    public String getRiskLevelProduction(@RequestParam(value = "ident", defaultValue = "") String id) {

        if (!id.isEmpty() && id.endsWith("H")) {

            return "high";

        }
        if (!id.isEmpty() && id.endsWith("L")) {

            return "low";

        }

        if (!id.isEmpty() && id.endsWith("M")) {

            return "medium";

        }

        if (!id.isEmpty()) {

            return "none";
        }

        return "none";
    }

    @RequestMapping(value = "/sdb/get", method = RequestMethod.GET, produces = "application/json")
    public String getRiskLevelSandbox(@RequestParam(value = "ident", defaultValue = "") String id) {

        if (!id.isEmpty() && id.endsWith("H")) {

            return "high";

        }
        if (!id.isEmpty() && id.endsWith("L")) {

            return "low";

        }

        if (!id.isEmpty()) {

            return "medium";
        }

        return "none";
    }
}
